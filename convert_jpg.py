from PIL import Image
import glob 
import os 

all_pngs = glob.glob('MURA-v1.1/*/*/*') 

for path in all_pngs: 
	current_path = os.path.join(os.getcwd(), path)
	image = Image.open(current_path) 
	rgb_image = image.convert('RGB')
	new_path = current_path[:-4] + '.jpg'
	rgb_image.save(new_path, "JPEG")
	os.remove(current_path)
