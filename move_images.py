import os
import shutil 
datasets = ['train', 'valid']
for d in datasets: 
    top_dir = os.path.join(os.getcwd(), 'MURA-v1.1', d)
    
    for label in os.listdir(top_dir):
        #print (label)
        if label == '.DS_Store':
            continue
        sub_dir = os.path.join(top_dir, label)
        #print (sub_dir)
        for p in os.listdir(sub_dir): 
            #print (p)
            if p == '.DS_Store':
                continue
            p_dir = os.path.join(sub_dir, p) 
            #print (p_dir)
            for o in os.listdir(p_dir):
                if o == '.DS_Store':
                    continue
                o_dir = os.path.join(p_dir, o)
                #print (o_dir)
                for img in os.listdir(o_dir):
                    if img == '.DS_Store':
                        continue
                    img_dir = os.path.join(o_dir, img)
                    name = p + '_'+ img
                    #print (name)
                    #print (os.path.join(sub_dir,name))
                    shutil.move(img_dir, os.path.join(sub_dir, name))
            
            shutil.rmtree(p_dir)
